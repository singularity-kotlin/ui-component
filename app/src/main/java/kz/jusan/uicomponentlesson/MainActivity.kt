package kz.jusan.uicomponentlesson

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val imageView: ImageView = findViewById(R.id.imageView)


        val button: Button = findViewById(R.id.button)
        button.setOnClickListener { button ->
            Toast.makeText(this, "button is clicked", Toast.LENGTH_SHORT).show()
            imageView.setImageResource(R.drawable.image1)
        }

        val checkBox: CheckBox = findViewById(R.id.checkBox)
        checkBox.setOnCheckedChangeListener{ view, isChecked ->
            Toast.makeText(this, "Checkbox is $isChecked", Toast.LENGTH_SHORT).show()
        }

        val editText: EditText = findViewById(R.id.editText)
        editText.addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(text: Editable?) {
                Toast.makeText(this@MainActivity, text, Toast.LENGTH_SHORT).show()
            }

        })

        val textView = findViewById<TextView>(R.id.textView)
        textView.text = getString(R.string.hello_again)
    }
}